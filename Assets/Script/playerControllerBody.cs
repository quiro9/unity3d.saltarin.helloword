﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerControllerBody : MonoBehaviour
{
    public GameObject game;
    public GameObject player;
    public GameObject enemyGenerator;

    // Start is called before the first frame update
    void Start()
    {        
    }

    // Update is called once per frame
    void Update()
    {  
    }

    void OnTriggerEnter2D(Collider2D other){
		if (other.gameObject.tag == "Enemy") {
            game.GetComponent<AudioSource>().Stop();
            player.SendMessage("UpdateState", "playerDie");
            enemyGenerator.SendMessage("CancelGenerator");
            game.GetComponent<game>().gameState = GameState.Ended;
		}
	}


}
