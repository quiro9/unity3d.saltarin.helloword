﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class playerController : MonoBehaviour
{

    public GameObject game;
	private Animator playerAnim;
    public AudioClip jumpClip;
    public AudioClip dieClip;

    public GameObject enemyGenerator;
    private AudioSource audioPlayer; 

    // Start is called before the first frame update
    void Start()
    {
        playerAnim = GetComponent<Animator>();
        audioPlayer = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
    }

	public void UpdateState(string state = null){
		if (state != null) {
			playerAnim.Play (state); 
            switch(state) {
                case "playerJump":
                    Debug.Log("ENTRA A JUMP!");
                    audioPlayer.clip = jumpClip;
                    audioPlayer.Play();
                break;
                case "playerDie":
                    Debug.Log("ENTRA A DIE!");
                    game.SendMessage("ResetTmeScale");
                    audioPlayer.clip = dieClip;
                    audioPlayer.Play();
                break;
            }           
		}
	}

    void GameEndReady(){
        // espera que todos los enemigos salgan...
        int EndCount = GameObject.FindGameObjectsWithTag("Enemy").Count();
        if (EndCount < 1){
            game.GetComponent<game>().gameState = GameState.EndReady;
        }
    }

}
