﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Security.Cryptography;



public class EnemyGeneratorController : MonoBehaviour
{

	public GameObject enemy0Prefab;
	public GameObject enemy1Prefab;

	private bool canInvokeEnemy = true;
	private bool firstInvoke = false;


    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
    }

	void CreateEnemy0()
	{
		Instantiate(enemy0Prefab, transform.position, Quaternion.identity);
		canInvokeEnemy = true;
	}

	void CreateEnemy1()
	{
		var Transform0 = new UnityEngine.Vector3(transform.position.x, transform.position.y+2.7f, transform.position.z);
		Instantiate(enemy1Prefab, Transform0, Quaternion.identity);
		canInvokeEnemy = true;
	}
	void CreateEnemy2()
	{
		var Transform0 = new UnityEngine.Vector3(transform.position.x, transform.position.y+1.45f, transform.position.z);
		Instantiate(enemy1Prefab, Transform0, Quaternion.identity);
		canInvokeEnemy = true;
	}
    public void StartGenerator(float generatorTimer) {
		//InvokeRepeating ("CreateEnemy", 1f, generatorTimer);
		if (canInvokeEnemy) {
			int typeEnemy = UnityEngine.Random.Range(0,5);
			if (typeEnemy == 1 && firstInvoke) {
				Invoke ("CreateEnemy1", generatorTimer);			
			} 
			else if (typeEnemy == 2 && firstInvoke) {
				Invoke ("CreateEnemy1", generatorTimer);			
			}
			else if (typeEnemy == 3 && firstInvoke) {
				Invoke ("CreateEnemy2", generatorTimer);			
			}
			else {
				Invoke ("CreateEnemy0", generatorTimer);				
			}
			Debug.Log("EnemyType: " + typeEnemy + "time: " + generatorTimer);
		}		canInvokeEnemy = false;
		firstInvoke = true;
	} 

	public void CancelGenerator() {
		firstInvoke = false;
		CancelInvoke ("CreateEnemy0");
		CancelInvoke ("CreateEnemy1");
		CancelInvoke ("CreateEnemy2");
	}
}
