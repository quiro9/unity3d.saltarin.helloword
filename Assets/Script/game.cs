﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.Diagnostics;
using UnityEngine.SceneManagement;
using Debug = UnityEngine.Debug;

public enum GameState {Idle, Playing, Ended, EndReady};

public class game : MonoBehaviour
{
	//[Range (0.02f,0.20f)]
	public float backgroundSpeed;
	public RawImage backgraund;
	public RawImage platform;
	public GameObject uiIdle;
	public GameObject uiScore;
	public GameObject uiEndScore;
	public GameObject uiEndGame;

	public Text ScoreTopText;
	public Text ScoreCenterText;

	public Text StartText;
	public Text StartInstruction;

	public GameState gameState = GameState.Idle;

	public GameObject player;
	public GameObject enemyGenerator;
	private bool pressScreen;

	private float enemyTimeRand = 1f;

	private AudioSource	musicPlayer;
	private float playerInitPosY;

	private float scaleTime;
	private float scaleInc;
	private float scaleIncMax;

	private int cointIncrementScore = 0;
	private int pointscore = 0;

    // Start is called before the first frame update
    void Start()
    {
		backgroundSpeed = 0.04f;
		scaleTime = 8f;
		scaleInc = 0.05f;
		scaleIncMax = 4.2f;
		musicPlayer = GetComponent<AudioSource>();
		// en mi caso player controller siempre esta en y (no se mueve realmente) por eso tomo el body (la imagen que si) dejo un pequeño margen de respuesta
		playerInitPosY  = (player.GetComponent<playerController>().GetComponentInChildren<Collider2D>().transform.position.y)+0.1f;		
		uiScore.SetActive(false);
		uiEndScore.SetActive(false);
		uiEndGame.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
		pressScreen = Input.GetKeyDown ("up") || Input.GetKeyDown ("space") || Input.GetMouseButtonDown (0);
		bool playerCanJump = player.GetComponent<playerController>().GetComponentInChildren<Collider2D>().transform.position.y <= playerInitPosY;
		enemyTimeRand = UnityEngine.Random.Range(1.3f,5f);
		
		// Empieza el juego
		if (gameState == GameState.Idle && pressScreen) {
			gameState = GameState.Playing;
			uiIdle.SetActive (false);
			uiScore.SetActive(true);

			player.SendMessage("UpdateState", "playerRun");
			InvokeRepeating("GameTimeScale", scaleTime, scaleTime);
			enemyGenerator.SendMessage("StartGenerator", enemyTimeRand);
			musicPlayer.Play();
		}
		else if (gameState == GameState.Playing) {
			ParallaxEffect ();
			IncrementScore();
			enemyGenerator.SendMessage("StartGenerator", enemyTimeRand);
			if (playerCanJump && pressScreen){
				player.SendMessage("UpdateState", "playerJump");
			}
		}
		else if (gameState == GameState.Ended) {
			uiScore.SetActive(false);
			uiEndScore.SetActive(true);
			ScoreCenterText.text = "tu puntaje fue de " + pointscore.ToString() + "m.";
			player.SendMessage("GameEndReady");
		}
		else if (gameState == GameState.EndReady) {
			uiEndGame.SetActive(true);
			if (pressScreen){
				RestartGame();
			}
		}
    }

	void ParallaxEffect() {
		float finalBackgroundSpeed = backgroundSpeed * Time.deltaTime;

		backgraund.uvRect = new Rect(backgraund.uvRect.x + finalBackgroundSpeed, 0.0f, 1.0f, 1.0f);
		platform.uvRect = new Rect(platform.uvRect.x + finalBackgroundSpeed * 4, 0.0f, 1.0f, 1.0f);
	}

	void IncrementScore(){
		cointIncrementScore += 1;
		if (cointIncrementScore%12 == 0){
			pointscore += 1;
			cointIncrementScore = 0;
		}
		ScoreTopText.text = pointscore.ToString() + "m.";
	}

	public void RestartGame(){
		uiEndScore.SetActive(false);
		uiEndGame.SetActive(false);
		uiIdle.SetActive(true);
		cointIncrementScore = 0;
		pointscore = 0;
		SceneManager.LoadScene("Principal");
	}

	void GameTimeScale(){
		if ( Time.timeScale < scaleIncMax){
			Time.timeScale += scaleInc;
		}
		Debug.Log("Time Increment! " + Time.timeScale);
	}

	void ResetTmeScale(){
		CancelInvoke("GameTimeScale");
		Time.timeScale = 1f;		
	}

}
